import React from 'react'
import Swal from "sweetalert2";
import {deleteMission, updateMission} from "../api-handler/missions-handler";

export default function MissionItem({mission}) {

    const missionType = () => {
        if (mission.is_human_visible === "1" && mission.is_zombie_visible === "1") {
            return "Global";
        }
        if (mission.is_human_visible === "0" && mission.is_zombie_visible === "1") {
            return "Zombie";
        }
        if (mission.is_human_visible === "1" && mission.is_zombie_visible === "0") {
            return "Human";
        }

    }

    const onUpdateClicked = async () => {
        await Swal.fire({
                title: 'Update Mission',
                html:
                    `<input id="swal-input1" value="${mission.mission_name}" placeholder="Mission name" class="swal2-input">` +
                    `<input id="swal-input2" value="${mission.is_human_visible}" placeholder="Is human? 1/0"  class="swal2-input">` +
                    `<input id="swal-input3" value="${mission.is_zombie_visible}" placeholder="Is zombie? 1/0" class="swal2-input">` +
                    `<input id="swal-input4" value="${mission.description}" placeholder="description" class="swal2-input">` +
                    `<input id="swal-input5" value="${mission.start_time}" placeholder="Start: yyyy-mm-dd hh:mm:ss" class="swal2-input">` +
                    `<input id="swal-input6" value="${mission.end_time}" placeholder="End: yyyy-mm-dd hh:mm:ss" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Update',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return updateMission(
                        mission.game_id,
                        mission.mission_id,
                        {
                            mission_name: document.getElementById('swal-input1').value,
                            is_human_visible: document.getElementById('swal-input2').value,
                            is_zombie_visible: document.getElementById('swal-input3').value,
                            description: document.getElementById('swal-input4').value,
                            start_time: document.getElementById('swal-input5').value,
                            end_time: document.getElementById('swal-input6').value,
                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Updated!',
                    text: 'The mission has been updated successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    const onDeleteClicked = async () => {
        await Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return deleteMission(mission.game_id, mission.mission_id)
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Deleted',
                    text: 'The mission has been deleted successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })

    }


    return (
        <>
            <p className="card_text">
                <button onClick={onUpdateClicked} type="button">Update</button>
                <button onClick={onDeleteClicked} type="button">Delete</button>
                ({missionType()}) {mission.mission_name} </p>
        </>


    )
}
