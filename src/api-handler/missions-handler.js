import axios from 'axios';
import {token, urlPrefix} from '../utils/global-variables'


export function getMissions(gameId) {

    return axios.get(`${urlPrefix()}/api/v1/game/${gameId}/mission`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('No content');
    })
}


export function updateMission(gameId,missionId,mission) {
    return axios.put(`${urlPrefix()}/api/v1/game/${gameId}/mission/${missionId}`, mission, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}

export function deleteMission(gameId,missionId) {
    return axios.delete(`${urlPrefix()}/api/v1/game/${gameId}/mission/${missionId}`, {
            headers: {
                'Authorization': `Bearer ${token()}`,
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}

export function addMission(gameId,mission) {

    return axios.post(`${urlPrefix()}/api/v1/game/${gameId}/mission`, mission, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}