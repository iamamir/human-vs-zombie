import React, {useContext, useEffect} from 'react'
import {getKills} from '../api-handler/kills-handler'
import KillItem from './KillItem';
import {AppContext} from "../context/AppContext";
import gravestone from "../assets/img/gravestone.png"
import {positions, types, useAlert} from "react-alert";


export default function Kills({gameId}) {

    const {kills, setKills} = useContext(AppContext);
    const alert = useAlert()

    useEffect(() => {
        getKills(gameId)
            .then(fetchedKills => setKills(fetchedKills))
            .catch((error) => {
                alert.show(`Kills Entry: ${error.message}`, {
                    type: types.INFO,
                    position: positions.BOTTOM_LEFT,
                })
            })
        const timer = setInterval(() => {
            getKills(gameId)
                .then(fetchedKills => setKills(fetchedKills))
                .catch((error) => {
                    //error handling
                    console.log(error)
                })
        }, 3000);
        return () => clearTimeout(timer);

    }, [gameId]);
    return (
        <li className={"cards_item"}>
            <div className={"card"}>
                <div className="card_image">
                    <img src={gravestone}/>
                </div>
                <div className="card_content" id="scroll">
                    <h2 className={"card_title"}> Kills Entry</h2>

                    {
                        kills.map(kill => <KillItem key={kill.id} kill={kill}> </KillItem>)
                    }
                </div>
            </div>

        </li>
    )
}
