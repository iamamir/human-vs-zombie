import React, {useContext} from 'react'
import {addMission} from '../api-handler/missions-handler'
import {AppContext} from "../context/AppContext";
import missionsIcon from "../assets/img/missionsIcon.png";
import MissionItem from "./MissionItem";
import Swal from "sweetalert2";


export default function Missions({gameId}) {

    const {missions} = useContext(AppContext);

    const onAddMissionClicked = async () => {
        await Swal.fire({
                title: 'Create Mission',
                html:
                    `<input id="swal-input1" placeholder="Mission name" class="swal2-input">` +
                    `<input id="swal-input2" placeholder="Is human? 1/0"  class="swal2-input">` +
                    `<input id="swal-input3" placeholder="Is zombie? 1/0" class="swal2-input">` +
                    `<input id="swal-input4" placeholder="description" class="swal2-input">` +
                    `<input id="swal-input5" placeholder="Start: yyyy-mm-dd hh:mm:ss" class="swal2-input">` +
                    `<input id="swal-input6" placeholder="End: yyyy-mm-dd hh:mm:ss" class="swal2-input">` +
                    `<input id="swal-input7" placeholder="Latitude" class="swal2-input">` +
                    `<input id="swal-input8" placeholder="Longitude" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Create',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return addMission(
                        gameId,
                        {
                            mission_name: document.getElementById('swal-input1').value,
                            is_human_visible: document.getElementById('swal-input2').value,
                            is_zombie_visible: document.getElementById('swal-input3').value,
                            description: document.getElementById('swal-input4').value,
                            start_time: document.getElementById('swal-input5').value,
                            end_time: document.getElementById('swal-input6').value,
                            latitude: document.getElementById('swal-input7').value,
                            longitude: document.getElementById('swal-input8').value

                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Created!',
                    text: 'The mission has been created successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }


    return (
        <li className={"cards_item"}>
            <div className={"card"}>
                <div className="card_image">
                    <img src={missionsIcon} alt="playerImage"/>
                </div>
                <div className="card_content" id="scroll">
                    <h2 className={"card_title"}>Missions</h2>
                    <button className="btn" onClick={onAddMissionClicked} type="button">Add mission</button>
                    {
                        missions.map(mission => <MissionItem key={mission.mission_id} mission={mission}/>)
                    }
                </div>
            </div>
        </li>
    )
}
