import axios from 'axios';
import {urlPrefix} from "../utils/global-variables";

export function login(loginCredentials) {

    return axios.post(`${urlPrefix()}/api/v1/login`, loginCredentials, {
            headers: {
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {

        return response.data;
    }).catch((error) => {
        throw new Error('Invalid login');
    })
}

export function userRegistration(useCredential) {

    return axios.post(`${urlPrefix()}/api/v1/user/register`, useCredential, {
            headers: {
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('Registration failure! Make sure email does not exist');
    })
}