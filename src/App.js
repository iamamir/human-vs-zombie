import './App.css';
import React from 'react'
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom'
import GameItemDetails from './views/GameItemDetails'
import Games from './views/Games'
import {AppProvider} from "./context/AppContext";
import Login from "./views/Login";
import Registration from "./views/Registration";


function App() {
    return (
        <BrowserRouter>
            <AppProvider>
                <div className="App">
                    <Switch>
                        <Route path="/register" component={Registration}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/games/:id" component={GameItemDetails}/>
                        <Route path="/games" component={Games}/>
                        <Route exact path="/">
                            <Redirect to="/login"/>
                        </Route>
                    </Switch>
                </div>
            </AppProvider>
        </BrowserRouter>);
}

export default App;
