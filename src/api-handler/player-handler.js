import axios from 'axios';
import {token, urlPrefix} from '../utils/global-variables'


export async function getCurrentPlayer(gameId) {
    return await axios.get(`${urlPrefix()}/api/v1/game/${gameId}/player/current`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('No content');
    })
}

export function getPlayer(gameId, playerId) {
    return axios.get(`${urlPrefix()}/api/v1/game/${gameId}/player/${playerId}`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('No content');
    })
}

export function getAllPlayers(gameId) {
    return axios.get(`${urlPrefix()}/api/v1/game/${gameId}/player`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('No content');
    })
}

export function updatePlayer(gameId,playerId,player) {
    return axios.put(`${urlPrefix()}/api/v1/game/${gameId}/player/${playerId}`, player, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}

export function deletePlayer(gameId,playerId) {
    return axios.delete(`${urlPrefix()}/api/v1/game/${gameId}/player/${playerId}`, {
            headers: {
                'Authorization': `Bearer ${token()}`,
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}

export function addPlayer(gameId,player) {

    return axios.post(`${urlPrefix()}/api/v1/game/${gameId}/player`, player, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}