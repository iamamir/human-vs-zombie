import React from 'react'
import logoutImg from '../assets/img/logout.png'
import {useHistory} from "react-router-dom";
import Swal from "sweetalert2";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";

export default function Header({children}) {

    const history = useHistory();

    const onHomeClicked = () => {
        history.push('/games')
    }
    const onLogOutClicked = () => {
        if (localStorage.getItem(LOCAL_STORAGE_NAME)) {
            Swal.fire({
                title: 'Are you sure you want to sign out?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, sign out!'
            }).then((result) => {
                if (result.isConfirmed) {
                    localStorage.removeItem(LOCAL_STORAGE_NAME)
                    history.push('/login')
                }
            })

        }
    }
    return (
        <div className="nav-wrapper">
            <div className="left-side">
                <div className="nav-link-wrapper">
                    <a onClick={onHomeClicked}>Home</a>
                </div>
                {children}
            </div>

            <div className="right-side">
                <div>
                    <img onClick={onLogOutClicked} className="logoutImg" src={logoutImg} alt="logout"/>
                </div>

            </div>
        </div>

    )
}
