export const token = () => {
    if (localStorage.getItem('hvz-token'))
        return JSON.parse(localStorage.getItem('hvz-token')).jwt
}

export const urlPrefix = () => {
    return 'http://localhost:8080'
    //return 'https://hvz-api-dk.herokuapp.com'
}
export const LOCAL_STORAGE_NAME = 'hvz-token';