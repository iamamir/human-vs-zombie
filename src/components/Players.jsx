import React, {useContext, useEffect} from 'react'
import {AppContext} from "../context/AppContext";
import playersImg from "../assets/img/players.png"
import {getAllPlayers} from "../api-handler/player-handler";
import PlayerItem from "./PlayerItem";
import {positions, types, useAlert} from "react-alert";


export default function Players({gameId}) {

    const {players, setPlayers} = useContext(AppContext)
    const alert = useAlert();
    useEffect(() => {

        getAllPlayers(gameId)
            .then(fetchedPlayers => setPlayers(fetchedPlayers))
            .catch(error => {
                    alert.show(`Players: ${error.message}`, {
                        type: types.INFO,
                        position: positions.BOTTOM_LEFT,
                    })
                }
            )

        const timer = setInterval(() => {
            getAllPlayers(gameId)
                .then(fetchedPlayers => setPlayers(fetchedPlayers))
                .catch(error =>
                    // error handling
                    console.log(error)
                )
        }, 3000);
        return () => clearTimeout(timer);
    }, [gameId]);


    return (
        <li className={"cards_item"}>
            <div className={"card"}>
                <div className="card_image">
                    <img src={playersImg} alt="playerImage"/>
                </div>
                <div className="card_content" id="scroll">
                    <h2 className={"card_title"}>Registered Players</h2>

                    {
                        players.map(player => <PlayerItem key={player.id} player={player}/>)
                    }
                </div>
            </div>

        </li>
    )
}
