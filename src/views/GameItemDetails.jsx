import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router';
import {getGame, updateGame} from '../api-handler/games-handler'
import Kills from "../components/Kills";
import PlayerProfileItem from "../components/PlayerProfileItem";
import GameMap from "../components/GameMap";
import {Redirect} from "react-router-dom";
import Players from "../components/Players";
import Header from "../components/Header";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";
import Missions from "../components/Missions";
import {positions, types, useAlert} from "react-alert";
import Swal from "sweetalert2";


export default function GameItemDetails() {

    const [game, setGame] = useState(null);
    let {id} = useParams();
    const loggedUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_NAME))
    const alert = useAlert()

    useEffect(() => {
        getGame(id)
            .then(game => setGame(game))
            .catch(error => {
                    alert.show(`Game: ${error.message}`, {
                        type: types.INFO,
                        position: positions.BOTTOM_LEFT,
                    })
                }
            )
        const timer = setInterval(() => {
            getGame(id)
                .then(game => setGame(game))
                .catch(error =>
                    //error handling
                    console.log(error)
                )
        }, 3000);
        return () => clearTimeout(timer);

    }, [id]);
    const onGameStatusClicked = async () => {
        await Swal.fire({
                title: 'Update game',
                html:
                    `<input id="swal-input1" value="${game.game_state}" placeholder="Game status" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Update',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return updateGame(
                        game.id,
                        {
                            game_name: game.game_name,
                            game_state: document.getElementById('swal-input1').value,
                            nw_latitude: game.nw_latitude,
                            nw_longitude: game.nw_longitude,
                            se_latitude: game.se_latitude,
                            se_longitude: game.se_longitude,
                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Game has been updated',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    return (
        <div className="container">
            {localStorage.getItem(LOCAL_STORAGE_NAME) ? '' : <Redirect to="/login"/>}
            <Header>
                {game &&
                <div className="nav-link-wrapper">
                    <a onClick={onGameStatusClicked}> Status: {game.game_state}</a>
                </div>
                }
            </Header>

            {game && <GameMap game={game}/>}
            <div className={"main"}>
                <ul className={"cards"}>
                    <Kills gameId={id}/>
                    {localStorage.getItem(LOCAL_STORAGE_NAME) && loggedUser.role === "admin" &&
                    <Missions gameId={id}/>
                    }
                    {game && localStorage.getItem(LOCAL_STORAGE_NAME) && loggedUser.role === "user" &&
                    <PlayerProfileItem gameId={id} gameStatus={game.game_state}/>
                    }
                    <Players gameId={id}/>
                </ul>
            </div>
        </div>
    )
}
