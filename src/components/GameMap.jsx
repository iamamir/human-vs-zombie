import React, {useEffect, useContext} from 'react'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet'
import {AppContext} from "../context/AppContext";
import {Icon} from "leaflet";
import gravestone from "../assets/img/gravestone.png"
import zombieMission from "../assets/img/zombiemissionicon.png"
import globalMission from "../assets/img/globalmissionicon.png"
import humanMission from "../assets/img/humanmissionicon.png"
import {getMissions} from "../api-handler/missions-handler";
import {positions, types, useAlert} from "react-alert";

export default function GameMap({game}) {
    const alert = useAlert()
    const gravestoneIcon = new Icon({
        iconUrl: gravestone,
        iconSize: [25, 25]
    })
    const zombieMissionIcon = new Icon({
        iconUrl: zombieMission,
        iconSize: [25, 25]
    })
    const humanMissionIcon = new Icon({
        iconUrl: humanMission,
        iconSize: [25, 25]
    })
    const globalMissionIcon = new Icon({
        iconUrl: globalMission,
        iconSize: [25, 25]
    })
    const {kills} = useContext(AppContext)
    const {players} = useContext(AppContext)

    const {missions, setMissions} = useContext(AppContext);

    useEffect(() => {

        getMissions(game.id)
            .then(fetchedMissions => setMissions(fetchedMissions))
            .catch(error => {
                alert.show(`Missions: ${error.message}`, {
                    type: types.INFO,
                    position: positions.BOTTOM_LEFT,
                })
            })

        const timer = setInterval(() => {
            getMissions(game.id)
                .then(fetchedMissions => setMissions(fetchedMissions))
                .catch(error => {
                    console.log(error.message)
                })
        }, 3000);

        return () => clearTimeout(timer);


    }, [game.id]);


    const getKillInfo = (kill) => {
        if (players.length <= 0) {
           return  alert.show(`Kills Entry: No active players`, {
                type: types.INFO,
                position: positions.BOTTOM_LEFT,
            })
        }
        if (kills.length <= 0) {
            return 'No kills to show';
        }
        /// Destructure
        const [killer] = players.filter(player => player.gameId === kill.gameId && player.id === kill.killerId)
        const [victim] = players.filter(player => player.gameId === kill.gameId && player.id === kill.victimId)
        if (!killer) {
            return 'Killer info not found';
        }
        if (!victim) {
            return 'Victim info not found';
        }
        // Now this is safe.
        return `${killer.userName} killed ${victim.userName}`
    }

    const missionIcon = (mission) => {
        if (mission.is_human_visible === "1" && mission.is_zombie_visible === "1") {
            return globalMissionIcon;
        }
        if (mission.is_human_visible === "0" && mission.is_zombie_visible === "1") {
            return zombieMissionIcon;
        }
        if (mission.is_human_visible === "1" && mission.is_zombie_visible === "0") {
            return humanMissionIcon;
        }

    }
    return (
        <MapContainer dragging={false} bounds={[
            [Number(game.nw_latitude), Number(game.nw_longitude)],
            [Number(game.se_latitude), Number(game.se_longitude)]
        ]} zoom={10} scrollWheelZoom={false}>
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png"
            />
            {kills.map(kill =>

                <Marker key={kill.id} position={[Number(kill.latitude), Number(kill.longitude)]}
                        icon={gravestoneIcon}
                >
                    <Popup>
                        <p className="card_text">{kill.story}</p>
                        <p className="card_text">{getKillInfo(kill)}</p>

                    </Popup>

                </Marker>
            )}


            {
                missions.map(mission => <Marker key={mission.mission_id}
                                                position={[Number(mission.latitude), Number(mission.longitude)]}
                                                icon={missionIcon(mission)}
                >
                    <Popup>
                        <h2 className="card_title"> {mission.mission_name}</h2>
                        <p className="card_text">{mission.description}</p>
                        <p className="card_text">{mission.start_time}</p>
                        <p className="card_text">{mission.end_time}</p>
                    </Popup>
                </Marker>)
            }

        </MapContainer>
    )
}
