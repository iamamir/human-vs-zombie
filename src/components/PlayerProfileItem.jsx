import React, {useContext, useEffect} from 'react'
import PlayerProfileItemImage from "./PlayerProfileItemImage";
import zombie from "../assets/img/zombie.PNG";
import bart from "../assets/img/bart.png";
import PlayerProfileItemKillForm from "./PlayerProfileItemKillForm";
import {AppContext} from "../context/AppContext";
import {getCurrentPlayer} from "../api-handler/player-handler";
import {positions, types, useAlert} from "react-alert";
import {useHistory} from "react-router-dom";

export default function PlayerProfileItem({gameId, gameStatus}) {
    const {currentPlayer, setCurrentPlayer} = useContext(AppContext)
    const alert = useAlert();
    const history = useHistory()
    useEffect(() => {
        getCurrentPlayer(gameId)
            .then(fetchedCurrentPlayer => {
                setCurrentPlayer(fetchedCurrentPlayer)
            })
            .catch(error => {
                alert.show(`Player: Join the game first`, {
                    type: types.INFO,
                    position: positions.BOTTOM_LEFT,
                })
                history.push('/games')

            })
        const timer = setInterval(() => {
            getCurrentPlayer(gameId)
                .then(fetchedCurrentPlayer => {
                    setCurrentPlayer(fetchedCurrentPlayer)
                })
                .catch(error => {
                    alert.show(`Player: Join the game first`, {
                        type: types.INFO,
                        position: positions.BOTTOM_LEFT,
                    })
                    history.push('/games')
                })
        }, 3000);
        return () => clearTimeout(timer);


    }, [gameId]);
    const playerImage = {
        image: currentPlayer.is_human === "1" && currentPlayer.is_patient_zero === "0" ? bart : zombie,
        alt: currentPlayer.is_human === "1" && currentPlayer.is_patient_zero === "0" ? "Human player" : "Zombie player",
    }

    return (
        <li className="cards_item">
            <div className="card">
                <div className="card_image">
                    <PlayerProfileItemImage image={playerImage.image} alt={playerImage.alt}/>
                    {currentPlayer.is_human === "1" && currentPlayer.is_patient_zero === "0" ?
                        <div className="card_content">
                            <h2 className="card_title"> {currentPlayer.userName}'s Profile</h2>
                            <p className="card_text">PatientZero's
                                state: {currentPlayer.is_patient_zero === "1" ? "true" : "false"}</p>
                            <p className="card_text">Bite Code: {currentPlayer.biteCode}</p>
                        </div>
                        :
                        <div className="card_content">
                            <h2 className="card_title"> {currentPlayer.userName}'s Profile</h2>
                            <p className="card_text">PatientZero's
                                state: {currentPlayer.is_patient_zero === "1" ? "true" : "false"}</p>
                            <PlayerProfileItemKillForm gameId={gameId} gameStatus={gameStatus}/>
                        </div>
                    }
                </div>
            </div>
        </li>
    )
}