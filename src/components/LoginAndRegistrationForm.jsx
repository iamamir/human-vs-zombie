import logoImage from '../assets/img/logo.jpg'
import {Redirect} from "react-router-dom";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";

export default function LoginAndRegistrationForm({children}) {


    return (
        <div className="fullSizeDiv">
            {localStorage.getItem(LOCAL_STORAGE_NAME) ? <Redirect to="/games"/> : ''}
            <div className="_cards">
                <figure className="_card _card--normal">
                    <div className="_card__image-container">
                        <img src={logoImage} alt="hvzlogo" className="_card__image"/>
                    </div>
                    <figcaption className="_card__caption">

                        {children}

                    </figcaption>
                </figure>

            </div>
        </div>
    )
}
