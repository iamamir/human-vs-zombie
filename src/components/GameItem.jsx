import React, {useContext} from 'react'
import {useHistory} from 'react-router-dom';
import {MapContainer, TileLayer} from "react-leaflet";
import {addPlayer, getCurrentPlayer} from "../api-handler/player-handler";
import {AppContext} from "../context/AppContext";
import {positions, types, useAlert} from 'react-alert'
import Swal from 'sweetalert2'
import {deleteGame, updateGame} from "../api-handler/games-handler";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";

export default function GameItem({game}) {
    const history = useHistory();
    const {setCurrentPlayer} = useContext(AppContext)
    const loggedUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_NAME))

    const alert = useAlert()

    const onOpenClicked = () => {
        if (loggedUser.role === "admin") {
            history.push(`/games/${game.id}`)
        } else {
            getCurrentPlayer(game.id)
                .then(fetchedCurrentPlayer => {
                    setCurrentPlayer(fetchedCurrentPlayer)
                    history.push(`/games/${game.id}`)
                    return alert.show("Open game success!", {
                        type: types.SUCCESS,
                        position: positions.BOTTOM_CENTER
                    })
                }).catch(error => {
                return alert.show("Please join the game first!", {
                    type: types.INFO,
                    position: positions.MIDDLE,
                })

            })
        }
    }
    const onJoinClicked = async () => {
        if (game.game_state !== "registration") {

            return alert.show("Join is allowed during registration only", {
                type: types.INFO,
                position: positions.BOTTOM_CENTER
            })
        } else {
            return  await Swal.fire({
                    title: 'Join game',
                    html:
                        `<input id="swal-input1" placeholder="Username" class="swal2-input">`,

                    showCancelButton: true,
                    confirmButtonText: 'Join',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return addPlayer(
                            game.id,
                            {
                                userName: document.getElementById('swal-input1').value,
                            })
                            .then(response => {
                                return response.data;
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }
            ).then((results) => {
                if (results.isConfirmed) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'You have joined the game successfully!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            })
        }
    }
    const onEditClicked = async () => {
        await Swal.fire({
                title: 'Update game',
                html:
                    `<input id="swal-input1" value="${game.game_name}" placeholder="name" class="swal2-input">` +
                    `<input id="swal-input2" value="${game.game_state}" placeholder="status"  class="swal2-input">` +
                    `<input id="swal-input3" value="${game.nw_latitude}" placeholder="nw latitude" class="swal2-input">` +
                    `<input id="swal-input4" value="${game.nw_longitude}" placeholder="nw longitude" class="swal2-input">` +
                    `<input id="swal-input5" value="${game.se_latitude}" placeholder="se latitude" class="swal2-input">` +
                    `<input id="swal-input6" value="${game.se_longitude}" placeholder="se longitude" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Update',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return updateGame(
                        game.id,
                        {
                            game_name: document.getElementById('swal-input1').value,
                            game_state: document.getElementById('swal-input2').value,
                            nw_latitude: document.getElementById('swal-input3').value,
                            nw_longitude: document.getElementById('swal-input4').value,
                            se_latitude: document.getElementById('swal-input5').value,
                            se_longitude: document.getElementById('swal-input6').value
                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Game has been updated',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    const onDeleteClicked = async () => {
        await Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return deleteGame(game.id)
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Deleted',
                    text: 'The game has been deleted successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })

    }

    const status = () => {
        switch (game.game_state) {
            case "registration":
                return "Status: Registration"
            case "in_progress":
                return "Status: In Progress"
            case "complete":
                return "Status: Complete"
            default:
                return "Status: Not Set"
        }
    }

    return (
        <li className={"cards_item"}>
            <div className={"card"}>
                <div className="card_image">
                    <MapContainer bounds={[
                        [Number(game.nw_latitude), Number(game.nw_longitude)],
                        [Number(game.se_latitude), Number(game.se_longitude)]
                    ]} zoom={10} scrollWheelZoom={false}>
                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                            url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png"
                        />
                    </MapContainer>
                </div>

                <div className={"card_content"}><h2 className={"card_title"}>{game.game_name}</h2>
                    <p className={"card_text"}>{status()}</p>
                    {loggedUser.role === "user" &&
                    <button type="button" className={"btn"} onClick={onJoinClicked}>Join</button>}
                    <button type="button" onClick={onOpenClicked} className={"btn"}>Open</button>
                    {loggedUser.role === "admin" &&
                    <button onClick={onEditClicked} type="button" className={"btn"}>Edit</button>}
                    {loggedUser.role === "admin" &&
                    <button type="button" onClick={onDeleteClicked} className={"btn"}>Delete</button>}
                </div>
            </div>

        </li>


    )
}
