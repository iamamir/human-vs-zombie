import React from 'react'

const PlayerProfileItemImage = ({ image, alt }) => (
    <>
        <img src={ image } alt={ alt } />
    </>
);
export default PlayerProfileItemImage