import React, {useState} from 'react'
import {login} from "../api-handler/login-register-handler";
import {useHistory} from "react-router-dom";
import LoginAndRegistrationForm from "../components/LoginAndRegistrationForm";
import {positions, types, useAlert} from 'react-alert'
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";

export default function Login() {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const history = useHistory();

    const alert = useAlert()

    const onEmailChange = (e) => {
        setEmail(e.target.value.trim())
    }
    const onPasswordChange = (e) => {
        setPassword(e.target.value.trim())
    }
    const loginClicked = () => {
        if (!email.includes("@")) {
            return alert.show("Invalid email", {
                type: types.ERROR,
                position: positions.BOTTOM_CENTER
            })
        }
        if (password.length <= 0) {
            return alert.show("Invalid password", {
                type: types.ERROR,
                position: positions.BOTTOM_CENTER
            })
        } else {
            login({email, password})
                .then(token => {
                    localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify(token))
                    history.push('/games')
                    return alert.show("Login success!", {
                        type: types.SUCCESS,
                        position: positions.BOTTOM_CENTER
                    })
                })
                .catch(error => {
                    return alert.show(error.message, {
                        type: types.ERROR,
                        position: positions.BOTTOM_CENTER
                    })
                })
        }

    }

    const onCreateAccountClicked = () => {
        history.push('/register')

    }
    return (
        <LoginAndRegistrationForm>
            <h1 className="_card__name">Zombies are coming...</h1>
            <button type="button" onClick={loginClicked} className="_card__type">Login</button>
            <input className="_card__input" onChange={onEmailChange} type="text" name="email"
                   placeholder=" Enter your email..."/>
            <input className="_card__input" onChange={onPasswordChange} type="password" name="password"
                   placeholder="Enter your password.."/>
            <p className="create_account" onClick={onCreateAccountClicked}>Create account </p>
        </LoginAndRegistrationForm>


    )
}
