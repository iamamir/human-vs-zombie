import axios from 'axios';
import {token,urlPrefix} from '../utils/global-variables'


export function getKills(gameId) {

    return axios.get(`${urlPrefix()}/api/v1/game/${gameId}/kill`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('No content');
    })
}

export function addKill(gameId, biteCode, kill) {
    const json = JSON.stringify(kill);

    return axios.post(`${urlPrefix()}/api/v1/game/${gameId}/kill/${biteCode}`, json, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },

            validateStatus: status => status === 200
        }
    ).then(response => {
        return `${response.data}: it went well`;
    }).catch((error) => {
        throw new Error('Something went wrong');
    })
}


export function updateKill(gameId,killId, kill) {
    return axios.put(`${urlPrefix()}/api/v1/game/${gameId}/kill/${killId}`, kill, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}

export function deleteKill(gameId,killId) {
    return axios.delete(`${urlPrefix()}/api/v1/game/${gameId}/kill/${killId}`, {
            headers: {
                'Authorization': `Bearer ${token()}`,
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}
