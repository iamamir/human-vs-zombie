import React, { createContext, useState } from 'react'

export const AppContext = createContext({
    games: [],
    kills:[],
    missions:[],
    players:[],
    squads:[],
    squadMembers:[],
    currentPlayer:{}
})

export function AppProvider(props) {
    const [ games, setGames ] = useState( [] )
    const [ kills, setKills ] = useState( [] )
    const [ missions, setMissions ] = useState( [] )
    const [ players, setPlayers ] = useState( [] )
    const [ squads, setSquads ] = useState( [] )
    const [ squadMembers, setSquadMembers ] = useState( [] )
    const [currentPlayer, setCurrentPlayer] = useState({})


    const AppState = {
        games,
        setGames,
        kills,
        setKills,
        missions,
        setMissions,
        players,
        setPlayers,
        squads,
        setSquads,
        squadMembers,
        setSquadMembers,
        currentPlayer,setCurrentPlayer
    }

    return (
        <AppContext.Provider value={ AppState }>
            { props.children }
        </AppContext.Provider>
    )
}
