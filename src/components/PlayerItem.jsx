import React from 'react'
import {deletePlayer, updatePlayer} from "../api-handler/player-handler";
import Swal from "sweetalert2";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";

export default function PlayerItem({player}) {

    const loggedUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_NAME))

    const onUpdateClicked = async () => {
        await Swal.fire({
                title: 'Update Player',
                html:
                    `<input id="swal-input1" value="${player.userName}" readonly placeholder="Username" class="swal2-input">` +
                    `<input id="swal-input2" value="${player.biteCode}" placeholder="Bite Code"  class="swal2-input">` +
                    `<input id="swal-input3" value="${player.is_human}" placeholder="Is human? 1/0" class="swal2-input">` +
                    `<input id="swal-input4" value="${player.is_patient_zero}" placeholder="Is patient zero? 1/0" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Update',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return updatePlayer(
                        player.gameId,
                        player.id,
                        {
                            userName: document.getElementById('swal-input1').value,
                            biteCode: document.getElementById('swal-input2').value,
                            is_human: document.getElementById('swal-input3').value,
                            is_patient_zero: document.getElementById('swal-input4').value
                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Updated!',
                    text: 'The player has been updated successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    const onDeleteClicked = async () => {
        await Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return deletePlayer(player.gameId, player.id)
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Deleted',
                    text: 'The player has been deleted successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })

    }

    const playerStatus = () => {
        if (player.is_human === "1") {
            return "(Human )"
        } else {
            return "(Zombie)"
        }
    }
    return (
        <>
            <p className="card_text" key={player.id}>
                {loggedUser.role === "admin" &&
                <button type="button" onClick={onUpdateClicked}>Update</button>}
                {loggedUser.role === "admin" &&
                <button type="button" onClick={onDeleteClicked}>Delete</button>}
                {
                loggedUser.role === "admin" && playerStatus()
            } {player.userName} </p>
        </>

    )
}
