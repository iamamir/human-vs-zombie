import React, {useContext, useEffect} from 'react'
import {addGame, getGames} from '../api-handler/games-handler'
import GameItem from '../components/GameItem';
import {AppContext} from "../context/AppContext";
import {Redirect} from "react-router-dom";
import Swal from "sweetalert2";
import Header from "../components/Header";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";
import {positions, types, useAlert} from "react-alert";

export default function Games() {

    const {games, setGames} = useContext(AppContext);
    const loggedUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_NAME))
    const alert = useAlert()

    useEffect(() => {
        getGames()
            .then(fetchedGames => setGames(fetchedGames))
            .catch(error => {
                alert.show(`Games: ${error.message}`, {
                    type: types.INFO,
                    position: positions.BOTTOM_LEFT,
                })
            })
        const timer = setInterval(() => {
            getGames()
                .then(fetchedGames => setGames(fetchedGames))
                .catch(error => {
                    console.log(error)
                })
        }, 3000);

        return () => clearTimeout(timer);
    }, []);

    const onCreatedGameClicked = async () => {
        await Swal.fire({
                title: 'Create Game',
                html:
                    `<input id="swal-input1" placeholder="Game Name" class="swal2-input">` +
                    `<input id="swal-input2" placeholder="NW Latitude"  class="swal2-input">` +
                    `<input id="swal-input3" placeholder="NW Longitude" class="swal2-input">` +
                    `<input id="swal-input4" placeholder="SE Latitude" class="swal2-input">` +
                    `<input id="swal-input5" placeholder="SE Longitude" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Create',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return addGame(
                        {
                            game_name: document.getElementById('swal-input1').value,
                            nw_latitude: document.getElementById('swal-input2').value,
                            nw_longitude: document.getElementById('swal-input3').value,
                            se_latitude: document.getElementById('swal-input4').value,
                            se_longitude: document.getElementById('swal-input5').value

                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Created!',
                    text: 'The game has been added successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    return (
        <div className="container">
            {localStorage.getItem(LOCAL_STORAGE_NAME) ? '' : <Redirect to="/login"/>}
            <Header>
                {localStorage.getItem(LOCAL_STORAGE_NAME) && loggedUser.role === "admin" &&
                <div className="nav-link-wrapper">
                    <a onClick={onCreatedGameClicked}>⊕ Game</a>
                </div>}
            </Header>

            <div className={"main"}>

                <ul className={"cards"}>
                    {games &&
                    games.map(game => <GameItem key={game.id} game={game}> </GameItem>)
                    }
                </ul>
            </div>
        </div>

    )
}
