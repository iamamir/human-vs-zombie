import React, {useState} from 'react'
import {addKill} from "../api-handler/kills-handler";
import {positions, types, useAlert} from "react-alert";

export default function PlayerProfileItemKillForm({gameId, gameStatus}) {
    const [biteCode, setBiteCode] = useState(null)
    const [killStory, setKillStory] = useState(null)
    const alert = useAlert()


    const onKillClicked = () => {
        if (gameStatus !== "in_progress") {
            return alert.show("Not allowed! games status is not in progress", {
                type: types.ERROR,
                position: positions.BOTTOM_CENTER
            })
        }
        const kill = {
            story: '',
            latitude: '',
            longitude: ''
        }
        if (biteCode && killStory) {

            if (biteCode.length >= 3 && killStory.length >= 3) {

                navigator.geolocation.getCurrentPosition(function (position) {
                        kill.latitude = position.coords.latitude.toString();
                        kill.longitude = position.coords.longitude.toString();
                        kill.story = killStory;
                        addKill(gameId, biteCode, kill).then(res => {
                                return alert.show("A Kill has been registered", {
                                    type: types.SUCCESS,
                                    position: positions.BOTTOM_CENTER
                                })
                            }
                        ).catch(error =>
                                //error handling
                            {
                                return alert.show(error.message, {
                                    type: types.ERROR,
                                    position: positions.BOTTOM_CENTER
                                })
                            }
                        )
                    },
                );
            }
        } else {
            return alert.show("Make sure the fields are not empty!", {
                type: types.INFO,
                position: positions.BOTTOM_CENTER
            })
        }
    }
    return (
        <>
            <input onChange={(e) => setKillStory(e.target.value.trim())} className="input" type="text"
                   placeholder="Tell the story..."/>
            <input onChange={(e) => setBiteCode(e.target.value.trim())} className="input" type="text"
                   placeholder="Enter victim's bitecode"/>
            <button onClick={onKillClicked} className="btn card_btn">Kill</button>
        </>
    )
}