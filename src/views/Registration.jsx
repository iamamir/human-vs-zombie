import React, {useState} from 'react'
import {userRegistration} from "../api-handler/login-register-handler";
import {useHistory} from "react-router-dom";
import LoginAndRegistrationForm from "../components/LoginAndRegistrationForm";
import {positions, types, useAlert} from 'react-alert'


export default function Registration() {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [matchingPassword, setMatchingPassword] = useState('')
    const history = useHistory();
    const alert = useAlert()

    const onEmailChange = (e) => {
        setEmail(e.target.value.trim())
    }
    const onPasswordChange = (e) => {
        setPassword(e.target.value.trim())
    }
    const onMatchPasswordChange = (e) => {
        setMatchingPassword(e.target.value.trim())
    }

    const registerClicked = () => {
        if (!email.includes("@")) {
            return alert.show("Invalid email", {
                type: types.ERROR,
                position: positions.BOTTOM_CENTER
            })
        }
        if (password.length <= 0 || password !== matchingPassword) {
            return alert.show("Please make sure you provide a password and it matches", {
                type: types.ERROR,
                position: positions.BOTTOM_CENTER
            })
        } else {
            userRegistration({email, password})
                .then(response => {
                    history.push('/login')
                    return alert.show("Registered", {
                        type: types.SUCCESS,
                        position: positions.BOTTOM_CENTER
                    })
                })
                .catch(error => {
                    return alert.show(error.message, {
                        type: types.ERROR,
                        position: positions.BOTTOM_CENTER
                    })
                })

        }


    }

    return (
        <LoginAndRegistrationForm>
            <button type="button" onClick={registerClicked} className="_card__type">Register</button>
            <input className="_card__input" onChange={onEmailChange} type="text" name="email"
                   placeholder=" Enter your email..."/>
            <input className="_card__input" onChange={onPasswordChange} type="password" name="password"
                   placeholder="Enter your password.."/>
            <input className="_card__input" onChange={onMatchPasswordChange} type="password" name="matchPassword"
                   placeholder="Re-enter your password.."/>
        </LoginAndRegistrationForm>


    )
}
