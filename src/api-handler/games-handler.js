import axios from 'axios';
import {token,urlPrefix} from '../utils/global-variables'

export async function getGames() {

    return await axios.get(`${urlPrefix()}/api/v1/game`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: () => true
        }
    ).then(response => {
        if (response.status === 200) {
            return response.data
        } else {
            throw new Error("No content")
        }
    })
        .catch(error => {
            return error.message
        })
}

export async function getGame(gameId) {
    return await axios.get(`${urlPrefix()}/api/v1/game/${gameId}`, {
            headers: {
                'Authorization': `Bearer ${token()}`
            },
            validateStatus: () => true
        }
    ).then(response => {
        if (response.status === 200) {
            return response.data
        } else {
            throw new Error("No content")
        }
    })
        .catch(error => {
            return error.message
        })
}

export function updateGame(gameId, game) {
    console.log(game)
    return axios.put(`${urlPrefix()}/api/v1/game/${gameId}`, game, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}

export function deleteGame(gameId) {
    return axios.delete(`${urlPrefix()}/api/v1/game/${gameId}`, {
            headers: {
                'Authorization': `Bearer ${token()}`,
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}
export function addGame(game) {

    return axios.post(`${urlPrefix()}/api/v1/game`, game, {
            headers: {
                'Authorization': `Bearer ${token()}`,
                'Content-Type': 'application/json'
            },
            validateStatus: status => status === 200
        }
    ).then(response => {
        return response.data;
    }).catch((error) => {
        throw new Error('Please try again');
    })
}
