import React, {useContext} from 'react'
import {AppContext} from "../context/AppContext";
import Swal from "sweetalert2";
import {deleteKill, updateKill} from "../api-handler/kills-handler";
import {LOCAL_STORAGE_NAME} from "../utils/global-variables";

export default function KillItem({kill}) {

    const {players} = useContext(AppContext)
    const loggedUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_NAME))
    const {currentPlayer} = useContext(AppContext)

    const onUpdateClicked = async () => {
        await Swal.fire({
                title: 'Update kill',
                html:
                    `<input id="swal-input1" value="${kill.story}" placeholder="Story" class="swal2-input">` +
                    `<input id="swal-input2" value="${kill.latitude}" placeholder="Latitude"  class="swal2-input">` +
                    `<input id="swal-input3" value="${kill.longitude}" placeholder="Longitude" class="swal2-input">` +
                    `<input id="swal-input4" value="${kill.timeOfDeath}" placeholder="Time of death" class="swal2-input">`,
                showCancelButton: true,
                confirmButtonText: 'Update',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return updateKill(
                        kill.gameId,
                        kill.id,
                        {
                            story: document.getElementById('swal-input1').value,
                            latitude: document.getElementById('swal-input2').value,
                            longitude: document.getElementById('swal-input3').value,
                            timeOfDeath: document.getElementById('swal-input4').value,
                            killerId: kill.killerId,
                            victimId: kill.victimId
                        })
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Updated!',
                    text: 'The kill has been updated successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    const onDeleteClicked = async () => {
        await Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return deleteKill(kill.gameId, kill.id)
                        .then(response => {
                            return response.data;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }
        ).then((results) => {
            if (results.isConfirmed) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Deleted',
                    text: 'The kill has been deleted successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })

    }

    const getKillInfo = () => {
        if (players.length <= 0) {
            return 'No active players';
        }
        /// Destructure
        const [killer] = players.filter(player => player.gameId === kill.gameId && player.id === kill.killerId)
        const [victim] = players.filter(player => player.gameId === kill.gameId && player.id === kill.victimId)
        if (!killer) {
            return 'Killer info not found';
        }
        if (!victim) {
            return 'Victim info not found';
        }
        // Now this is safe.
        return `${killer.userName} killed ${victim.userName}`
    }

    const updateButton = () => {
        if (currentPlayer.id === kill.killerId) {
            return (
                <>
                    <button type="button" onClick={onUpdateClicked}>Update</button>
                </>
            )
        }

        if (localStorage) {
            if (loggedUser.role === "admin") {
                return (
                    <>
                        <button type="button" onClick={onUpdateClicked}>Update</button>
                    </>
                )
            }
        }
    }

    return (
        <>
            <p className={"card_text"}>
                {updateButton()}
                {localStorage.getItem(LOCAL_STORAGE_NAME) && loggedUser.role === "admin" &&
                <button type="button" onClick={onDeleteClicked}>Delete</button>}
                {getKillInfo()}

            </p>

        </>


    )
}
